package com.example.l7task1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.l7task1.model.Person;

import static com.example.l7task1.MainActivity.KEY;

public class PersonActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);
        textView = (TextView) findViewById(R.id.name);

        if (getIntent() != null) {
            Person person = (Person) getIntent().getSerializableExtra(KEY);
            textView.setText(person.getName());
        }
    }
}
