package com.example.l7task1.model;


import android.support.annotation.ColorInt;

public class Person {

    private String name;
    private String eMail;
    private String address;

    @ColorInt
    private int color;

    public Person(String name, String eMail, String address, int color) {
        this.name = name;
        this.eMail = eMail;
        this.address = address;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getEMail() {
        return eMail;
    }

    public String getAddress() {
        return address;
    }

    public int getColor() {
        return color;
    }
}
