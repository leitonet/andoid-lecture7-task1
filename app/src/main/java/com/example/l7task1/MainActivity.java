package com.example.l7task1;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.l7task1.model.Person;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements PersonAdapter.PersonClickListener {

    public static final String KEY = "person";

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        LinearLayoutManager ll = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(ll);

        PersonAdapter adapter = new PersonAdapter(this, generate(), this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(Person person) {
        Intent intent = new Intent(this, PersonActivity.class);
        intent.putExtra(KEY, person);
        startActivity(intent);
    }

    private List<Person> generate(){
        List<Person> list = new ArrayList<>();
        for(int i = 0; i < 15; i++){
            list.add(new Person("Jake"+i, "W"+i, "USA",
                    Color.parseColor("#0000"+Integer.toHexString(i)+""+Integer.toHexString(i))));
        }
        return list;
    }

}
