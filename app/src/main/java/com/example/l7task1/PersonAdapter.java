package com.example.l7task1;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.l7task1.model.Person;

import java.util.List;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.ViewHolder>{

    interface PersonClickListener {
        void onClick(Person person);
    }

    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<Person> list;
    private PersonClickListener personClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            personClickListener.onClick(list.get(position));
        }
    };

    public PersonAdapter(Context context, List<Person> list, PersonClickListener personClickListener) {
        this.context = context;
        this.list = list;
        this.personClickListener = personClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(item, itemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Person person = list.get(position);
        holder.name.setText(person.getName());
        holder.eMail.setText(person.getEMail());
        holder.address.setText(person.getAddress());
        holder.icon.setBackgroundColor(person.getColor());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView eMail;
        TextView address;
        View icon;

        ItemClickListener itemClickListener;

        public ViewHolder(View item, final ItemClickListener itemClickListener){
            super(item);
            this.itemClickListener = itemClickListener;
            name = (TextView) item.findViewById(R.id.name);
            eMail = (TextView) item.findViewById(R.id.e_mail);
            address = (TextView) item.findViewById(R.id.address);
            icon = item.findViewById(R.id.rectangle);
            item.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }

    }
}
